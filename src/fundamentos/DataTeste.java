package fundamentos;

public class DataTeste {

    public static void main(String[] args) {

        Data d1 = new Data();
        d1.dia = 15;
        d1.mes = 12;
        d1.ano = 1988;

        Data d2 = new Data();
        d2.dia = 10;
        d2.mes = 01;
        d2.ano = 2004;

        System.out.println(d1.obterDataFormatada());
        System.out.println(d2.obterDataFormatada());


    }
}