package fundamentos;

import java.util.Scanner;

public class Desafio {
    public static void main(String[] args) {
        Scanner entrada = new Scanner(System.in);

        String valor = "";

        while (!valor.equalsIgnoreCase("sair")){
            System.out.println("Digite seu nome: ");
            valor = entrada.nextLine();
        }
        entrada.close();
    }
}
